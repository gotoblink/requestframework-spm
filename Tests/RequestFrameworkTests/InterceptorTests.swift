import Foundation

import XCTest
@testable import RequestFramework

class InterceptorTests: XCTestCase {
    static var counter = 0
    
    /**
     Test the interceptor runner
     */
    func testInterceptorRunner() {
        InterceptorTests.counter = 0
        
        let request = Request(url: URL(string: "http://google.com")!, method: .GET, body: nil, headers: HeaderCollection(), interceptors: [
            TestInterceptor(),
            TestInterceptor()
        ], timeout: nil)
        
        let runner = InterceptorRunner(request: request)
        
        runner.runBefore {
            
        }
        
        XCTAssertEqual(InterceptorTests.counter, 2)
        
    }
}

class TestInterceptor : Interceptor {
    func after(request: Request, response: Response?, next: @escaping (Response?) -> Void) {
        InterceptorTests.counter += 1
        
        next(response)
    }
    
    func before(request: Request, next: () -> Void) {
        InterceptorTests.counter += 1
        
        next()
    }
    
    func after(request: Request, response: Response, next: (Response) -> Void) {
        InterceptorTests.counter += 1
        
        next(response)
    }
}
