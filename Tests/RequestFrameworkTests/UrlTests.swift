import Foundation

import XCTest
@testable import RequestFramework

class UrlTests: XCTestCase {
    /**
     Test the url concatenation
     */
    func testUrlConcatenation() {
        // Test all the cases that url concatenation should handle
        XCTAssertEqual(Url.concat(base: "http://example.com", path: "path"), "http://example.com/path")
        XCTAssertEqual(Url.concat(base: "http://example.com/", path: "path"), "http://example.com/path")
        XCTAssertEqual(Url.concat(base: "http://example.com", path: "/path"), "http://example.com/path")
        XCTAssertEqual(Url.concat(base: "http://example.com/", path: "/path"), "http://example.com/path")
        XCTAssertEqual(Url.concat(base: "http://example.com/", path: "/path/"), "http://example.com/path/")
        XCTAssertEqual(Url.concat(base: "/http://example.com/", path: "/path"), "/http://example.com/path")
        XCTAssertEqual(Url.concat(base: "/http://example.com/", path: "/path/"), "/http://example.com/path/")
        XCTAssertEqual(Url.concat(base: "", path: "/path"), "/path")
        XCTAssertEqual(Url.concat(base: "base", path: ""), "base")
        XCTAssertEqual(Url.concat(base: "base", path: "/"), "base")
        XCTAssertEqual(Url.concat(base: "", path: "/"), "")
        XCTAssertEqual(Url.concat(base: "", path: ""), "")
        
        XCTAssertEqual(Url.urlEncode([
            ("key1", "value1")
        ]), "key1=value1")
        XCTAssertEqual(Url.urlEncode([
            ("key1", "value1"),
            ("key1", "value1")
            ]), "key1=value1&key1=value1")
        XCTAssertEqual(Url.urlEncode([
            ("key1", "value1"),
            ("key2", "value2")
            ]), "key1=value1&key2=value2")
        XCTAssertEqual(Url.urlEncode([
            ("key1", "value1&")
            ]), "key1=value1%26")
        XCTAssertEqual(Url.urlEncode([
            ("key1", "value1/")
            ]), "key1=value1%2F")
    }
}
