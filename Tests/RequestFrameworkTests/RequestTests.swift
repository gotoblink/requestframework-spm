import Foundation

import XCTest
@testable import RequestFramework

class RequestTests: XCTestCase {
    
    func testPathParamters() {
        let urlString = RequestBuilder.createDefault().method(.GET).baseUrl("http://base").path("{part1}/{part2}", placeholders: [
            "{part1}": "part1",
            "{part2}": "part2"
            ]).build().url.absoluteString
        
        XCTAssertEqual(urlString, "http://base/part1/part2")
    }
    
    /**
     Test a simple GET request
     */
    func testGet() {
        let expectation1 = self.expectation(description: "GET Request")
        let expectation2 = self.expectation(description: "GET Request 2")
        
        // Create a simple get request
        RequestBuilder.createDefault().baseUrl("https://jsonplaceholder.typicode.com").get(path: "/posts").execute().subscribe { (response, error) in
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            
            XCTAssertEqual(response!.status, 200)
            XCTAssertNotNil(response!.data)
            
            XCTAssertNotNil(response!.jsonArray())
            
            //XCTAssertEqual(response!.jsonArray()?.first!["userId"] as! Int, 1)
            
            XCTAssertGreaterThan(response!.headers.count, 0)
            
            expectation1.fulfill()
        }
        
        // Create a simple get request
        RequestBuilder.createDefault().url("https://jsonplaceholder.typicode.com/posts/1").method(.GET).execute().subscribe { (response, error) in
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            
            XCTAssertEqual(response!.status, 200)
            XCTAssertNotNil(response!.data)
            
            XCTAssertNotNil(response!.jsonObject())
            
            XCTAssertEqual(response!.jsonObject()!["userId"] as! Int, 1)
            
            XCTAssertGreaterThan(response!.headers.count, 0)
            
            XCTAssertEqual(response!.headers["content-type"], "application/json; charset=utf-8")
            
            expectation2.fulfill()
        }
        
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    
    /**
     Test a simple GET request with an interceptor
     */
    func testInterceptor() {
        let expectation1 = self.expectation(description: "GET Request")
        
        
        
        // Create a simple get request
        RequestBuilder.createDefault().baseUrl("https://jsonplaceholder.typicode.com").interceptor(TestInterceptor2()).get(path: "/posts").execute().subscribe { (response, error) in
            XCTAssertNotNil(response)
            XCTAssertNil(error)
            
            XCTAssertEqual(response!.status, 200)
            XCTAssertNotNil(response!.data)
            
            XCTAssertNotNil(response!.jsonArray())
            
            //XCTAssertEqual(response!.jsonArray()?.first!["userId"] as! Int, 1)
            
            XCTAssertGreaterThan(response!.headers.count, 0)
            
            
            XCTAssertEqual(response?.headers["test"], "test_value")
            
            expectation1.fulfill()
        }
        
        self.waitForExpectations(timeout: 10, handler: nil)
    }
}

/**
 Interceptor to use for testing
 */
class TestInterceptor2: Interceptor {
    func after(request: Request, response: Response?, next: @escaping (Response?) -> Void) {
        response?.headers.add("test", "test_value")
        next(response)
    }
    
    func before(request: Request, next: () -> Void) {
        next()
    }
    
    func after(request: Request, response: Response, next: (Response) -> Void) {
        response.headers.add("test", "test_value")
        next(response)
    }
}
