import Foundation

import XCTest
@testable import RequestFramework

class BasicParameterSerializerTests: XCTestCase {
    /**
     Test the url concatenation
     */
    func testSerializer() {
        let serializer = BasicParameterSerializers()
        
        XCTAssertTrue(serializer.canSerialize(type: String.self))
        XCTAssertTrue(serializer.canSerialize(type: Int.self))
        XCTAssertTrue(serializer.canSerialize(type: Int16.self))
        XCTAssertTrue(serializer.canSerialize(type: Int64.self))
        XCTAssertTrue(serializer.canSerialize(type: Double.self))
        XCTAssertTrue(serializer.canSerialize(type: Float.self))
        XCTAssertTrue(serializer.canSerialize(type: Bool.self))
        
        XCTAssertFalse(serializer.canSerialize(type: UserDefaults.self))
        
        // Test all the cases that url concatenation should handle
        XCTAssertEqual(serializer.serialize(value: "Test"), "Test")
        XCTAssertEqual(serializer.serialize(value: 1), "1")
        XCTAssertEqual(serializer.serialize(value: 1.1), "1.1")
        XCTAssertEqual(serializer.serialize(value: Float(1.1)), "1.1")
        
        XCTAssertEqual(serializer.serialize(value: Int16(23)), "23")
        XCTAssertEqual(serializer.serialize(value: Int32(23)), "23")
        XCTAssertEqual(serializer.serialize(value: Int64(23)), "23")
    }
}
