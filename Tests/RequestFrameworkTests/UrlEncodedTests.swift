import Foundation

import XCTest
@testable import RequestFramework

class UrlEncodedTests: XCTestCase {
    func testUrlEncoded() {
        var urlEncoded = UrlEncodedBody([
            ("key", "value")
        ])
        
        XCTAssertEqual(String(data: urlEncoded.serialize(), encoding: String.Encoding.utf8), "key=value")
        
        urlEncoded = UrlEncodedBody([
            ("key", "value"),
            ("key", "value")
        ])
        
        XCTAssertEqual(String(data: urlEncoded.serialize(), encoding: String.Encoding.utf8), "key=value&key=value")
        
        urlEncoded = UrlEncodedBody([
            ("key", "value"),
            ("key2", "value2")
            ])
        
        XCTAssertEqual(String(data: urlEncoded.serialize(), encoding: String.Encoding.utf8), "key=value&key2=value2")
        
        
        urlEncoded = UrlEncodedBody([])
        
        XCTAssertEqual(String(data: urlEncoded.serialize(), encoding: String.Encoding.utf8), "")
    }
}
