import Foundation

/**
 An interceptor factory is used for providing interceptors for requests
 */
public protocol InterceptorFactory {
    /**
     This is called when an interceptor is needed for a request
     */
    func create() -> Interceptor
}
