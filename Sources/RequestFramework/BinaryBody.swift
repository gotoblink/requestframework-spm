//
//  Created by Warrick Walter on 6/01/17.
//  Copyright © 2017 Digitalstock. All rights reserved.
//

import Foundation

/**
 Represents an arbituary binary body
 */
public class BinaryBody : RequestBody {
    init(_ data: Data, contentType: String = "application/octet-stream") {
        self.data = data
        self.contentTypeValue = contentType
    }
    
    let data: Data
    let contentTypeValue: String
    
    public func serialize() -> Data {
        return data
    }
    
    public func contentType() -> String {
        return contentTypeValue
    }
}
