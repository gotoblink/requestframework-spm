import Foundation

/**
 Defines the valid http methods for requests
 */
public enum HttpMethod: String {
    case GET
    case POST
    case PUT
    case PATCH
    case DELETE
}
