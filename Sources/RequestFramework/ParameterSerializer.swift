import Foundation

/**
 Defines a protocol for serializing parameters to strings
 */
public protocol PropertySerializer {
    func canSerialize(type: Any.Type) -> Bool
    func serialize(value: Any) -> String
}
