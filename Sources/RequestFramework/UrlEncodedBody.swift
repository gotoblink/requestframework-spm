import Foundation
import JsonSerializer

/**
 Defines a protocol that classes can implement to become serializeable to json
 */
public protocol UrlEncodedSerializableClass {
    func urlEncode() -> [(String, Any)]
}

/**
 Url form encoded body
 */
public class UrlEncodedBody : RequestBody {
    var parameters: [(String, String)]
    
    /**
     Create a url encoded body from a list of parameters
     */
    public init(_ parameters: [(String, String)]) {
        self.parameters = parameters
    }
    
    public init() {
        parameters = []
    }
    
    /**
     Create a url encoded body from a url encoded serializable body
     */
    public init(object: UrlEncodedSerializableClass) {
        // Set the parameters from the encoded paramters of a url encodable class
        // Serialize the values with the request builder serializers
        self.parameters = object.urlEncode().map { (keyValue) -> (String, String) in
            return (keyValue.0, RequestBuilder.serializeValue(keyValue.1))
        }
    }
    
    /**
     Add a key and parameter to the parameters
     */
    public func add(_ key: String, _ value: String) {
        parameters.append((key, value))
    }
    
    public func serialize() -> Data {
        return Url.urlEncode(parameters).data(using: String.Encoding.utf8)!
    }
    
    public func contentType() -> String {
        return "application/x-www-form-urlencoded"
    }
}
