import Foundation

/**
 Represents the default values to use for every request
 */
public class RequestDefaults {
    public init() {
        
    }
    
    /**
     Paramter serializers for serializing request parameters
     */
    public var serializers: [PropertySerializer] = [
        BasicParameterSerializers()
    ]
    
    /**
     The default timeout to use for all requests
     */
    public var timeout: Double?
    
    /**
     The default base url to use for all requests
     */
    public var baseUrl: String?
    
    /**
     The default headers to use for all requests
     */
    public var headers = HeaderCollection()
    
    /**
     The interceptors to add to every request
     */
    public var interceptors = [InterceptorFactory]()
    
    /**
     Create a request builder the uses all the default values
     */
    func requestBuilder() -> RequestBuilder {
        let builder = RequestBuilder()
        
        // If there is a timeout, set the requests timeout
        if let timeout = timeout {
            _ = builder.timeout(timeout)
        }
        
        // Set the base url, if one is specified
        if let baseUrl = baseUrl {
            _ = builder.baseUrl(baseUrl)
        }
        
        // Set all the headers
        for header in headers {
            _ = builder.header(header.0, header.1)
        }
        
        for interceptor in interceptors {
            _ = builder.interceptor(interceptor.create())
        }
        
        return builder
    }
}
    
