import Foundation

public extension Response {
    /**
     Returns the response data as a json object dictionary
     */
    public func jsonObject() -> [String: Any?]? {
        if let data = self.data {
            do {
                return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String: Any]
            }
            catch {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    /**
     Returns the response data as a json array dictionary
     */
    public func jsonArray() -> [Any]? {
        if let data = self.data {
            do {
                return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [Any]
            }
            catch {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    /**
     Returns the response data as a json array dictionary
     */
    public func jsonObjectArray() -> [[String: Any?]]? {
        if let data = self.data {
            do {
                return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [[String: Any]]
            }
            catch {
                return nil
            }
        }
        else {
            return nil
        }
    }
}
