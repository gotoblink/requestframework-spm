import Foundation

/**
 Defines a protocol for intercepting and controling a request as it runs
 */
public protocol Interceptor {
    /**
     This function is called before the request runs.
     This function will return a contination, which must be triggered when the request can proceed
     */
    func before(request: Request, next: @escaping () -> Void)
    
    /**
     This function is called after the request has run
     This method should return continuation that specifies whether the request should be re-run or not
     */
    func after(request: Request, response: Response?, next: @escaping (Response?) -> Void)
}
