//
//  Created by Warrick Walter on 9/01/17.
//  Copyright © 2017 Digitalstock. All rights reserved.
//

import Foundation

/**
 Multipart content disposition parameters
 */
public class DispositionParameter {
    public init(_ key: String, _ value: String) {
        self.key = key
        self.value = value
    }
    
    public init(_ key: String) {
        self.key = key
        self.value = nil
    }
    
    /**
     The key for this disposition parameter
     */
    let key: String
    
    /**
     The value for this disposition parameter
     */
    let value: String?
    
    /**
     Create a name disposition parameter
     */
    static public func name(_ name: String) -> DispositionParameter {
        return DispositionParameter("name", name)
    }
    
    /**
     Create a filename disposition parameter
     */
    static func filename(_ filename: String) -> DispositionParameter {
        return DispositionParameter("filename", filename)
    }
}

/**
 Content disposition parameters
 */
enum DispositionParameters: String {
    case attachment
    case formData = "form-data"
    case signal
    case alert
    case icon
    case render
    case recipientListHistory = "recipient-list-history"
    case session
    case alib
    case earlySession = "early-session"
    case recipientList = "recipient-list"
    case notification
    case byReference = "by-reference"
    case intoPackage = "into-package"
    case recordingSession = "recording-session"
}
