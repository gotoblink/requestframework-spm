import Foundation
import JsonSerializer

/**
 Json request body to serialize json objects as request bodies
 */
public class JsonRequestBody : RequestBody {
    /**
     Create a json request body from a json serializable class
     */
    public init(object: JsonSerializable) {
        self.json = JsonSerializer.serializeObject(object: object)
    }
    
    /**
     Create a json request body from json dictionary/array
     */
    public init(_ json: Any) {
        self.json = JsonSerializer.serializeObject(object: json)
    }
    
    /**
     The serialized json
     */
    let json: Any
    
    public func serialize() -> Data {
        do {
            return try JSONSerialization.data(withJSONObject: JsonSerializer.serializeObject(object: json), options: JSONSerialization.WritingOptions(rawValue: 0))
        }
        catch {
            fatalError("Failed to serialize json data: \(error)")
        }
    }
    
    public func contentType() -> String {
        return "application/json"
    }
}

public extension RequestBuilder {
    /**
     Add a json serializable object as the body of the request
     */
    public func body(_ object: JsonSerializable) -> RequestBuilder {
        _ = self.body(JsonRequestBody(object: object))
        return self
    }
}
