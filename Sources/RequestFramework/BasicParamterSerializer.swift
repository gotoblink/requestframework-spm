import Foundation

/**
 Basic paramter serializers for primitive swift types such as Int, Double, Float, Bool
 */
class BasicParameterSerializers : PropertySerializer {
    /**
     Return all the parameters that can be serialized
     */
    func canSerialize(type: Any.Type) -> Bool {
        
        if type == String.self {
            return true
        }
        if type == Int.self || type == Int16.self || type == Int32.self || type == Int64.self {
            return true
        }
        else if type == Double.self {
            return true
        }
        else if type == Float.self {
            return true
        }
        else if type == Bool.self {
            return true
        }
        else {
            return false
        }
    }
    
    /**
     Serialize a parameter value
     */
    func serialize(value: Any) -> String {
        let type = Swift.type(of: value)
        
        if type == String.self {
            return value as! String
        }
        else if type == Int.self || type == Int16.self || type == Int32.self || type == Int64.self {
            if type == Int.self {
                return String(value as! Int)
            }
            else if type == Int16.self {
                return String(value as! Int16)
            }
            else if type == Int32.self {
                return String(value as! Int32)
            }
            else if type == Int64.self {
                return String(value as! Int64)
            }
            else {
                fatalError("Attempted to serialize an unsupported type")
            }
        }
        else if type == Double.self {
            return String(value as! Double)
        }
        else if type == Float.self {
            return String(value as! Float)
        }
        else if type == Bool.self {
            return String(value as! Bool)
        }
        else {
            fatalError("Attempted to serialize an unsupported type")
        }
    }
}
