import Foundation

/**
 Request body protocol
 */
public protocol RequestBody {
    /**
     This should return the data for the request body
     */
    func serialize() -> Data
    
    /**
     This must return the content-type that should be used in the headers for this request
     */
    func contentType() -> String
}
