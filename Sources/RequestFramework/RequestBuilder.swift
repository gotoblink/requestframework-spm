import Foundation

/**
 Builder for creating requests
 */
public class RequestBuilder {
    /**
     The default parameters to use for all requsts
     */
    public static var defaults = RequestDefaults()
    
    private var url: String?
    private var baseUrl: String?
    private var method: HttpMethod?
    private var timeout: Double?
    private var body: RequestBody?
    private var headers = HeaderCollection()
    private var interceptors = [Interceptor]()
    private var queryParameters: [(String, String)]?
    
    /**
     Create a request builder using the default values specified in RequestBuilder.defaults
     */
    public static func createDefault() -> RequestBuilder {
        // Get the the RequestBuilder from the default values
        return RequestBuilder.defaults.requestBuilder()
    }
    
    public static func create(defaults: RequestDefaults) -> RequestBuilder {
        // Get the the RequestBuilder from the default values
        return defaults.requestBuilder()
    }
    
    public init() {
        
    }
    
    /**
     Add an interceptor to this request
     */
    public func interceptor(_ interceptor: Interceptor) -> RequestBuilder {
        interceptors.append(interceptor)
        return self
    }
    
    /**
     Serialize a paramter using all the available parameter serializers
     */
    static func serializeValue(_ value: Any) -> String {
        // Attempt to serialize the value for the querystring
        for serializer in RequestBuilder.defaults.serializers {
            if serializer.canSerialize(type: type(of: value)) {
                return serializer.serialize(value: value)
            }
        }
        
        // Throw an error if the type could not be serialized
        fatalError("No serializer capable of serializing the provided type, which was \(type(of: value))")
    }
    
    /**
     Add a querystring parameter to this request
     */
    public func query(_ key: String, value: Any) -> RequestBuilder {
        queryParameters = queryParameters ?? []
        
        queryParameters?.append((key, RequestBuilder.serializeValue(value)))
        return self
    }
    
    /**
     Set the absolute url for this request
     */
    public func url(_ url: String) -> RequestBuilder {
        self.url = url
        return self
    }
    
    /**
     Set a request header - If overwrite is true (by default) then any existing headers with the same key will be overwritten
     */
    public func header(_ key: String, _ value: String, _ overwrite: Bool = true) -> RequestBuilder {
        headers.add(key, value, overwrite)
        return self
    }
    
    /**
     Set the timeout for this request
     */
    public func timeout(_ timeout: Double) -> RequestBuilder {
        self.timeout = timeout
        return self
    }
    
    /**
     Set the method for this request
     */
    public func method(_ method: HttpMethod) -> RequestBuilder {
        self.method = method
        return self
    }
    
    /**
     Create a builder that will do a GET on the base url with the provided path
     */
    public func get(path: String) -> RequestBuilder {
        _ = self.path(path)
        self.method = .GET
        return self
    }
    
    /**
     Create a builder that will do a GET on the base url with the provided path
     */
    public func get(path: String, placeholders: [String: Any]) -> RequestBuilder {
        _ = self.path(path, placeholders: placeholders)
        self.method = .GET
        return self
    }
    
    /**
     Create a simple post request with path paramters
     */
    public func post(path: String, placeholders: [String: Any]) -> RequestBuilder {
        _ = self.path(path, placeholders: placeholders)
        self.method = .POST
        return self
    }
    
    /**
     Create a simple post request with a body and path placeholders
     */
    public func post(path: String, body: RequestBody?, placeholders: [String: Any]) -> RequestBuilder {
        _ = self.path(path, placeholders: placeholders)
        self.method = .POST
        self.body = body
        return self
    }
    
    /**
     Make a basic post request
     */
    public func post(path: String, body: RequestBody?) -> RequestBuilder {
        _ = self.path(path)
        self.method = .POST
        self.body = body
        return self
    }
    
    
    /**
     Make a basic patch request
     */
    public func patch(path: String, body: RequestBody?) -> RequestBuilder {
        _ = self.path(path)
        self.method = .PATCH
        self.body = body
        return self
    }
    
    /**
     Make a basic post request with no body
     */
    public func post(path: String) -> RequestBuilder {
        _ = self.path(path)
        self.method = .POST
        return self
    }
    
    /**
     Set the base url to use for this request (the final url will be this + the request path)
     */
    public func baseUrl(_ url: String?) -> RequestBuilder {
        self.baseUrl = url
        return self
    }
    
    /**
     Set the path for this request (the final url will be the baseUrl + the request path)
     */
    public func path(_ path: String, placeholders: [String: Any]) -> RequestBuilder {
        // Reduce the path by replacing and serializing all the paramters
        self.url = placeholders.reduce(path) { (last, current) -> String in
            return last.replacingOccurrences(of: current.key, with: RequestBuilder.serializeValue(current.value))
        }
        
        return self
    }
    
    /**
     Set the path for this request (the final url will be the baseUrl + the request path)
     */
    public func path(_ path: String) -> RequestBuilder {
        self.url = path
        return self
    }
    
    /**
     Set the body for this request (only usefull for HTTP methods that have a body, eg: POST or PUT)
     */
    public func body(_ data: RequestBody) -> RequestBuilder {
        self.body = data
        
        return self.header("content-type", data.contentType())
    }
    
    /**
     Build the url from the absolute url, or the base url and path
     */
    private func buildUrl() -> URL {
        var url = ""
        
        if baseUrl != nil && self.url != nil {
            url = Url.concat(base: baseUrl!, path: self.url!)
        }
        else if self.url != nil {
            url = self.url!
        }
        else if baseUrl != nil {
            url = baseUrl!
        }
        else {
            fatalError("Invalid request url, no url was provided. You must provide either a url, or a base url")
        }

        
        // If there are querystring parameters, encode them here
        if let queryParameters = queryParameters {
            url += "?" + Url.urlEncode(queryParameters)
        }
        
        let urlObject = URL(string: url)
        if urlObject == nil {
            fatalError("Invalid url")
        }
        
        return urlObject!
    }
    
    /**
     Build the request from this RequestBuilder
     */
    public func build() -> Request {
        // Build the url
        let url = buildUrl()
        
        // Check that a method has been supplied
        if self.method == nil {
            fatalError("No method was specified for this request")
        }
        
        
        return Request(url: url, method: method!, body: body, headers: self.headers, interceptors: self.interceptors, timeout: timeout)
    }
    
    /**
     Build the request, and execute it - returning the response
     */
    public func execute() -> PendingResponse {
        return build().execute()
    }
}
