import Foundation

public class Request {
    public var url: URL
    public var method: HttpMethod
    public var body: RequestBody?
    public var headers: HeaderCollection
    public var interceptors: [Interceptor]
    public var timeout: Double?
    
    init(url: URL) {
        self.url = url
        self.method = .GET
        self.headers = HeaderCollection()
        self.interceptors = []
    }
    
    init(url: URL, method: HttpMethod, body: RequestBody?, headers: HeaderCollection, interceptors: [Interceptor], timeout: Double?) {
        self.url = url
        self.body = body
        self.method = method
        self.headers = headers
        self.interceptors = interceptors
        self.timeout = timeout
    }
    
    
    /**
     Execute this request, returning a pending response
     */
    public func execute() -> PendingResponse {
        // Create the response object for this request
        let response = PendingResponse()
        
        // Fire the before interceptors - which will go on to fire the request when the before callbacks have completed
        let interceptorRunner = InterceptorRunner(request: self)
        
        interceptorRunner.runBefore {
            // Run the request
            self.executeRequest(interceptorRunner: interceptorRunner, pendingResponse: response)
        }
        
        return response
    }
    
    private func executeRequest(interceptorRunner: InterceptorRunner, pendingResponse: PendingResponse) {
        // Build the request
        var request = URLRequest(url: url)
        
        // Set the method of the request
        request.httpMethod = method.rawValue
        
        if let timeout = self.timeout {
            request.timeoutInterval = timeout
        }
        
        // Add the header values
        for header in headers {
            request.setValue(header.1, forHTTPHeaderField: header.0)
        }
        
        
        if let body = body {
            // Set the body of the request
            request.httpBody = body.serialize()
            
            // Add the content type from the serializable body
            request.setValue(body.contentType(), forHTTPHeaderField: "content-type")
        }
        
        
        // Record the time that this request starts
        let startTime = CFAbsoluteTimeGetCurrent()
    
        // Create the data task
        let dataTask = URLSession.shared.dataTask(with: request) { (data, res, error) in
            if let error = error {
                DispatchQueue.main.async {
                    // The request had an error, so send the pending response back as complete, with the error
                    interceptorRunner.runAfter(response: nil, completion: { (response) in
                        // Pass on the response
                        pendingResponse.responseReceived(response: nil, error: error)
                    })
                }
            }
            else {
                let httpResponse = res as! HTTPURLResponse
                
                // The headers from the request response
                let headers = HeaderCollection()
                
                httpResponse.allHeaderFields.forEach({ (header) in
                    if let key = header.key as? String, let value = header.value as? String {
                        headers.add(key.lowercased(), value, false)
                    }
                })
                
                let requestDuration = CFAbsoluteTimeGetCurrent() - startTime
                
                // Build the response to the request
                let response = Response(request: self, statusCode: httpResponse.statusCode, headers: headers, data: data, requestDuration: requestDuration)
                
                // Run the interceptors on the main thread
                DispatchQueue.main.async {
                    interceptorRunner.runAfter(response: response, completion: { (response) in
                        // Pass on the response
                        pendingResponse.responseReceived(response: response, error: nil)
                    })
                }
            }
        }
        
        // Start the data task
        dataTask.resume()
    }
}
