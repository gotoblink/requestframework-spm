import Foundation

/**
 Url helpers
 */
class Url {
    /**
     Concat a base url with a url path
     */
    static func concat(base: String, path: String) -> String {
        var url = ""
        
        // If the base url ends with a slash, substring it to remove the slash
        if base.last == "/" || base.last == "\\" {
            url = String(base.prefix(base.count - 1))
        }
        else {
            // The base url doesn't have trailing slashes, so use it directly
            url = base
        }
        
        // Only if there is a path and it isn't the root
        if path != "" && path != "/" && path != "\\" {
            // Add the concatenating slash
            url += "/"
            
            // If the path url starts with a leading slash, substring it to remove it
            if path.first == "/" || path.first == "\\" {
                url += String(path.suffix(path.count - 1))
            }
            else {
                // The base url doesn't have trailing slashes, so use it directly
                url += path
            }
        }
        
        return url
    }
    
    /**
     Url encode parameters
     */
    static func urlEncode(_ parameters: [(String, String)]) -> String {
        // Reduce the body
        let bodyString = parameters.reduce("", { (last, current) -> String in
            let start = last == "" ? "" : (last + "&")
            
            return start + current.0.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)! + "=" + current.1.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        })
        
        return bodyString
    }
}
