//
//  Created by Warrick Walter on 17/01/17.
//  Copyright © 2017 Digitalstock. All rights reserved.
//

import Foundation

public class DelayInterceptorFactory: InterceptorFactory {
    private let delay: Double
    
    public init(delay: Double) {
        self.delay = delay
    }
    
    public func create() -> Interceptor {
        return DelayInterceptor(delay: delay)
    }
}

/**
 Delay interceptor to aid debugging
 */
public class DelayInterceptor : Interceptor {
    private let delay: Double
    
    public init(delay: Double) {
        self.delay = delay
    }
    
    public func before(request: Request, next: @escaping () -> Void) {
        next()
    }
    
    public func after(request: Request, response: Response?, next: @escaping (Response?) -> Void) {
        // Delay the response by x amount of time
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: {
            next(response)
        })
    }
}
