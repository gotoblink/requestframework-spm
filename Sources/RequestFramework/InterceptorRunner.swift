import Foundation

/**
 Interceptor runner for firing interceptors
 */
class InterceptorRunner {
    let interceptors: [Interceptor]
    let request: Request
    
    init(request: Request) {
        self.interceptors = request.interceptors
        self.request = request
    }
    
    /**
     Run all the before interceptors
     */
    func runBefore(_ completion: @escaping () -> Void) {
        func runBefore(interceptors: [Interceptor], completion: @escaping () -> Void) {
            if let interceptor = interceptors.first {
                // Make the interceptors mutable
                var interceptors = interceptors
                
                // Remove the first interceptor, since we are about to run it
                interceptors.remove(at: 0)
                
                interceptor.before(request: request, next: {
                    runBefore(interceptors: interceptors, completion: completion)
                })
            }
            else {
                completion()
            }
        }
        
        runBefore(interceptors: interceptors, completion: completion)
    }
    
    /**
     Run all the after interceptors
     */
    func runAfter(response: Response?, completion: @escaping (Response?) -> Void) {
        func runAfter(interceptors: [Interceptor], response: Response?, completion: @escaping (Response?) -> Void) {
            if let interceptor = interceptors.first {
                // Make the interceptors mutable
                var interceptors = interceptors
                
                // Remove the first interceptor, since we are about to run it
                interceptors.remove(at: 0)
                
                interceptor.after(request: request, response: response, next: { (response) in
                    runAfter(interceptors: interceptors, response: response, completion: completion)
                })
            }
            else {
                completion(response)
            }
        }
        
        runAfter(interceptors: interceptors, response: response, completion: completion)
    }
}
