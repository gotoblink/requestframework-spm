import Foundation

/**
 A pending response represents a request that is still running, and waiting for a response
 */
public class PendingResponse {
    /**
     The response to this request, if the request has received a response - or nil if the request doesn't yet have a response
     */
    var response: Response?
    
    /**
     The error that this request finished with
     */
    var error: Error?
    
    /**
     This returns true if this response has been completed, and will either have a succesfull response or an error
     */
    var isComplete: Bool {
        get {
            return response != nil || error != nil
        }
    }
    
    /**
     This is called when the request has generated a response/error - this makes this pending response considered 'complete', since it has received a response
     */
    func responseReceived(response: Response?, error: Error?) {
        self.response = response
        self.error = error
        
        // Fire all the callbacks
        onCompleteCallbacks.forEach { (callback) in
            callback(self.response, self.error)
        }
        
        // Remove all the callbacks, since they have been fired (this is for ARC)
        onCompleteCallbacks.removeAll()
    }
    
    /**
     List of the callbacks to fire when the request completes
     */
    private var onCompleteCallbacks = [(Response?, Error?) -> Void]()
    
    /**
     Subscribe a callback that receives the response and error
     */
    public func subscribe(_ callback: @escaping (Response?, Error?) -> Void) {
        if isComplete {
            callback(response, error)
        }
        else {
            onCompleteCallbacks.append(callback)
        }
    }
    
    /**
     Subscribe success and error callback handlers
     */
    public func subscribe(_ successCallback: @escaping (Response) -> Void, _ errorCallback: @escaping (Error) -> Void) {
        let delegationCallback = { (response: Response?, error: Error?) in
            if let response = response {
                successCallback(response)
            }
            else {
                errorCallback(self.error!)
            }
        }
        
        // Subscribe the delegation callback
        subscribe(delegationCallback)
    }
}
