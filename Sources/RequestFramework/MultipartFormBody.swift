//
//  Created by Warrick Walter on 6/01/17.
//  Copyright © 2017 Digitalstock. All rights reserved.
//

import Foundation

public class MultipartFormBody: RequestBody {
    public init() {
        
    }
    
    var content = [(RequestBody, [DispositionParameter])]()
    
    /**
     Add a content body to this multipart form body
     */
    public func add(_ body: RequestBody, dispositionParameters: [DispositionParameter] = []) {
        if body is MultipartFormBody {
            fatalError("You cannot add a Multipart Form Body to a Multipart Form Body")
        }
        
        content.append((body, dispositionParameters))
    }
    
    /**
     Add a file content
     */
    public func file(_ body: RequestBody, name: String, filename: String) {
        content.append((body, [DispositionParameter("form-data"), DispositionParameter.name(name), DispositionParameter.filename(filename)]))
    }
    
    /**
     The form boundary to use
     */
    var formBoundary = "RequestFramework-" + UUID().uuidString
    
    public func serialize() -> Data {
        var bodyData = Data()
        
        // Write the form boundary
        func boundary(last: Bool = false) {
            if last {
                bodyData.append("\r\n--\(formBoundary)--\r\n".data(using: .utf8)!)
            }
            else {
                bodyData.append("\r\n--\(formBoundary)\r\n".data(using: .utf8)!)
            }
        }
        
        // Write a header
        func header(key: String, value: String) {
            bodyData.append((key + ": " + value + "\r\n").data(using: .utf8)!)
        }
        
        // Write body data
        func body(_ data: Data) {
            bodyData.append("\r\n".data(using: .utf8)!)
            bodyData.append(data)
        }
        
        var i = 0
        for b in content {
            // Write the boundary, header, and body
            boundary()
            
            var dispositionHeader = ""
                
            for i in 0..<b.1.count {
                if i > 0 {
                    dispositionHeader += " "
                }
                
                if let val = b.1[i].value {
                    dispositionHeader += "\(b.1[i].key)=\"\(val)\";"
                } else {
                    dispositionHeader += "\(b.1[i].key);"
                }
            }
            
            header(key: "Content-Disposition", value: dispositionHeader)
            header(key: "Content-Type", value: b.0.contentType())

            body(b.0.serialize())
            
            i += 1
        }
        
        boundary(last: true)
        
        // Return the string encoded with utf-8
        return bodyData
    }
    
    public func contentType() -> String {
        return "multipart/form-data; boundary=\(formBoundary)"
    }
}
