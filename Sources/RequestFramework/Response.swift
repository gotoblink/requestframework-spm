import Foundation

/**
 This object represents the result of a request
 */
public class Response {
    public let request: Request
    public var status: Int
    public var headers: HeaderCollection
    public var data: Data?
    public let requestDuration: TimeInterval
    
    /**
     Create the response for this request
     */
    init(request: Request, statusCode: Int, headers: HeaderCollection, data: Data?, requestDuration: TimeInterval) {
        self.request = request
        self.status = statusCode
        self.headers = headers
        self.data = data
        self.requestDuration = requestDuration
    }
}
