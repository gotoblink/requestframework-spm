//
//  Created by Warrick Walter on 30/11/16.
//  Copyright © 2016 Digitalstock. All rights reserved.
//

import Foundation

public class HeaderCollection : Sequence {
    var headers = [(key: String, value: String)]()
    
    /**
     Get the count of headers
     */
    public var count: Int {
        get {
            return headers.count
        }
    }
    
    /**
     Add a header value
     */
    public func add(_ key: String, _ value: String, _ overwrite: Bool = true) {
        if overwrite {
            remove(key)
        }
        
        headers.append((key, value))
    }
    
    /**
     Clear all values for a header key
     */
    public func remove(_ key: String) {
        for i in stride(from: headers.count - 1, through: 0, by: -1) {
            if headers[i].0 == key {
                headers.remove(at: i)
            }
        }
    }
    
    /**
     Get the first header value for the key
     */
    public subscript(key: String) -> String? {
        get {
            for header in headers {
                if header.0 == key {
                    return header.1
                }
            }
            return nil
        }
        set(newValue) {
            if let newValue = newValue {
                add(key, newValue, true)
            }
        }
    }
    
    public func makeIterator() -> IndexingIterator<[(key: String, value: String)]> {
        return headers.makeIterator()
    }
}
