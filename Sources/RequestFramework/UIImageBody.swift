//
//  Created by Warrick Walter on 6/01/17.
//  Copyright © 2017 Digitalstock. All rights reserved.
//

import Foundation
import UIKit

/**
 */
public class UIImageBody : RequestBody {
    public init(_ image: UIImage, format: UIImageBodyFormat = .png) {
        self.image = image
        self.format = format
    }
    
    let format: UIImageBodyFormat
    let image: UIImage
    
    public func serialize() -> Data {
        switch format {
        case .png:
            return image.pngData() ?? Data()
        case .jpg:
            return image.jpegData(compressionQuality: 100) ?? Data()
        }
    }
    
    public func contentType() -> String {
        switch format {
        case .png:
            return "image/png"
        case .jpg:
            return "image/jpeg"
        }
    }
}

public enum UIImageBodyFormat {
    case png
    case jpg
}
