// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "RequestFramework",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "RequestFramework",
            targets: ["RequestFramework"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "JsonSerializer", url: "https://Wira_wan@bitbucket.org/gotoblink/jsonserializer-spm.git", branch: "master"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "RequestFramework",
            dependencies: ["JsonSerializer"]),
        .testTarget(
            name: "RequestFrameworkTests",
            dependencies: ["RequestFramework"]),
    ]
)
